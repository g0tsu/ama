[<< Table of contents](README.md)

# Using GPG

  * [Create a new key](#create-a-new-key)
  * [Create a subkey](#create-a-subkey)
  * [Import public key](#import-someones-public-key)
  * [Decrypt file](#decrypt-file-someone-sent-to-you)
  * [My key](#public-gpg-key)


If there's a reason to use encryption for messages or simple sign some files
you share over untrusted services, you've to install [gnupg](https://www.gnupg.org/software/index.html)

To make life a bit easier, I'd share some useful commandline tips:

## Create a new key

To generate a new private/public key:

```
gpg --full-generate-key
... follow the prompt
```

## List of available keys:

```
gpg -k
gpg -K

```

the last one is for private keys

## Create a subkey

```
gpg --edit-key 0xMYKEY

> addkey
... follow the prompt
```

Export subkey (check its id by `gpg -k`):

```
gpg -a --export 0xSUBKEY
```

## Import someones public key

```
gpg --import somekey.asc
```

Encrypt file using someones public key (for an instance mine):
```
gpg --encrypt -r g0tsu@dnmx.org secretfile.txt
```

## Decrypt file someone sent to you

```
gpg --decrypt secretfile.txt.gpg
```

## Public GPG Key

You can download [g0tsu.asc](g0tsu.asc) file, or copy it as a text:


```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQGNBGGwz6cBDAC8c5CK/i4rF9U30XfTclRrwoJ0wVW2S8TKVpGFIozhgdLQMNdR
QJPzn4lWFhXkngDg9bG3zUd6kqlKh4/jPgRHsDmObNuu6/5i/my/QOhIyP9P8QGM
3dXSi1ui26OYZeOkUEL5NbrGKQCx9lBlfPxUiKCmTgpfXZdORlDIOQQxqiHtCAEx
b5/CsHG8C3VFyayBHYy4qpw9Ls9W+GmOvsiMjCQqWyXiwZHsHMXfKw+JxDwalDt8
AAmFKP7vGwd6pVqVQP9LplG3DV+rKCRJ+j6OW1a5vib6Xjrx0LNzC390IzjZqDHy
kecp0LRz2sj7NQbQzSX6ifcrfWqDcPBkDee548HKSq0KwH/Uossjs3AaiW7PhWJn
0Hrn6Sv7VPNr8T1CNyTSbPKvP2VoAnhOenZB64yo0otT1OeNaws8uZdeRa1c25UG
FPjfric8H8ln9g550801sMMy2MGw1zzfoiqpil0aC2RyoJojxPt0JOcfHOzYMGxp
A1HDAuW5TY6j10sAEQEAAbQWZzB0c3UgPGcwdHN1QGRubXgub3JnPokB1AQTAQgA
PhYhBJZJnFP/a+G0NrfsOyFNpJwiyPvxBQJhsM+nAhsDBQkDwmcABQsJCAcCBhUK
CQgLAgQWAgMBAh4BAheAAAoJECFNpJwiyPvx26sMALMWozTO0vei4/GAsi6A2ulv
cKkJ6+IA/+S/Lc5dsqpwFBFa3KIma03Qx6Pg19nLf6ZE3qCFlTZbyJLP5TKdsd0c
vT3gQp1oTg/mUeGV1f1qinfG1v3EJLfwsa2SYQofgn/+w7e4P15ddbzSEtmw1gQp
rpXjdtX/bUe+JNL3Wz6djSyx0BPBHSGPF/0BUbafA2+oPBO8uw9QiBMfMIFD+zZv
IRdCl1Qj/Sxg6YCVxaqNai84GM1rfJmF50bR4Esh7IJzxkcTtIPw7/tQyjIqJ0MU
ddKNzeZdO08EbRWfwsFd3/IrrCC9B7FQGsgkTZpXP4UEsIR5WNypLozu3l107wZv
VuHht64D2LqQdA+tWd0n3fNYZb1zoH/z1OHrP3VokqWP9bFA/9enrfPplyfbjsXa
tSYr0ybYFT8HOvXOe7Z32SbMBk2X+0rkOv6SyL75ySlKkGibz0PhhFyxluIqIsY0
lwIBjKbwEOcHiDDEpcd4TPktG0a6EY2KujN2gA2s/bkBjQRhsM+nAQwAtC7JDbLA
hdPL+naedt8meoH9anpiz7xiz8+gMcL3QZBh1YMzl8O9OegcMTx9Hs8+SrUGaXUu
DjB0KlBSho8sMUnxgTTyMbjv7T2+y9OSDIfbMmcA9cbdSd9fwsB0gA+Nu5bdU8WS
vLHXvJS0x5x9zq0VnolfSaiPfTlwcpzQU4hbeco2hiDHfRpfqWSlRf/5UjaTe2Z3
oMyIQ+2phn5uCeNvqT44gF0uT1aVz29So2LELs+/WZxJf7WlbK8RNvYgR7jjlS5Q
GehrzEYLMFWijb2fggRKdYLDn9/ZoOiYZ+Uu1WYhl1ySG9vw0whFL/lt0p+3jm1B
kVMRDnn2fQi4LvDNFSf9S4Kh//qNGoER5s0U97EK9s+hza6rhaXckhP8Ev1h+hrB
q+B7ygOnixXuMx9IBX0BwO3ePJXgZCJNTxoh+q18J9I6ijPw177CWk4jRDqQarvw
XZg0KzhW725tSFbcSxHzMaZvwGWw7dSYy9DKWL46KUl7m6MOU4S5KdXtABEBAAGJ
AbwEGAEIACYWIQSWSZxT/2vhtDa37DshTaScIsj78QUCYbDPpwIbDAUJA8JnAAAK
CRAhTaScIsj78TPtC/9Acqq6QOO6x/FtLy1wsmflh5XjpJ1um8bQZ5YYwW3Nw1Gu
fuPrDEJWyYFfrQIfo7M79XUAq5cOK99A/vK/O0IqOQ4h7E5bpfB38D0/dPP2pR4C
LuiK8lUTm2SJajSwYBRcp7zo0uzlpL+/POeEYGI2yJftZuGUIbW5XM62L4DtvlPW
kLNwPIl8lcg2IJPspEpPlCdewJ1Nwuqt9BgFemP2xOKuySbVvHlnKEDzn/Z2uwFF
Aevs30cQaImnHUGZ0MT7Emd7y4FNn4rm2rv3bneT6Stc2iiEwcJq0ULJ/ZmiACYS
Yv9vlKeJ5wkgCtvgwvzObqPgzSxRBrWHAPW18cJ8M+Itxt+JTtGDZgw1bdOcS1wf
1io+VBARrHmQ9L5P1y15MKbtIoL+0pig0vuj2r7RB5d0p6kImqspqw7Z0ccghjD/
LQnCwAKDDwEq250chKXIqa4fGuuQQsOyC8mHvLnJdC0CYC20qjbpcptr88K1yM0x
mgr3U68HRT88BdTvqei4jQRhsNGSAQQAx9jW1wXfUt0puMzXgDOQnsIVZNTsyrSj
3TmbFeY55dZBzXY5jkEW91SnpKUTAUNYHtRqmYT8iosyxfo6Xi1I06K4vvO8tZxY
X12gPNsbVDM4jeIUYUKGDE8DYZTSyH4/jI3Ta5QDngK8Nuls/OWnz/nWrS58amHb
RqNvVkN8bBkAEQEAAYkBvAQYAQgAJhYhBJZJnFP/a+G0NrfsOyFNpJwiyPvxBQJh
sNGSAhsMBQkJZgGAAAoJECFNpJwiyPvxP7AL/3YVbd2qb7zrs/2Zgu0p45PYyPxa
t+VD4lh/LHtuGZzbnIC68c9locJzE5a736WeU3ctSe/B6DrELFNpoYzzShAc1INM
FJlnjy6MEZ0no8MnH9Zi4ncscs3f5+WhLCedE7YCNqUXvGtJvMwgq7/RXyGgipoL
af2SGveLGn9ACnweWKpOKquP/uPaisReteJnIl1k/8bnFY7DQdV1vacF2HIWeUeF
AvzeB4x2+XA8s6euAlh7MM3/DjOHbQxLI2qG3Oe1gC95IigD8ufkDYdZk7Ild+M0
Dd4J5E79bIBlmYXJMvAknx9413x/YRsBnJgfH+YrfvC2MPZzuR1gDXGrJEPbrFKP
FBV7yxxbUo0ECV61U4RR0veYWWacPreX16i1FhGAIMPKLrRrm1CotBcPr1g4UjRS
JFBl/4c3qQKvm+5N/iPogX1aCT5YsY1+TwgMSbSpwExa2r8YS9wMZXHP40NACnxD
r7MoCNBNS+2o2IqlkG6oMPyVltUzEGlftX6fLg==
=xzar
-----END PGP PUBLIC KEY BLOCK-----
```

[<< Table of contents](README.md)
