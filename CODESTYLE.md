[<< Table of contents](README.md)

# Code of Conduct

## Curly brackets

It is not a standard for `C` but I prefer this way:

Instead of:

```
void func()
{
....
}
```

Please use:

```
void func() {
....
}
```

## Unix style

If you have a great 70 inch plasma tv.. with netflix.. oh

Instead of such crazy style:

```

        if (v == 1) {^M
                printf("v = 1\n");^M

                if (b == 2)^M
                      printf("b = 2\n");^M
        }^M
```

Please use two, neat spaces:

```
  if (v == 1) {
    printf("v = 1\n");

    if (b == 2)
      printf("b = 2\n");
  }
```

