# AMA

![chase](DATA/g913.png)

> The chase is better than the catch. Btw, how much of the fish ?

## Table of contents

  * [List of Projects](PROJECTS.md)
  * [Hiring information](HIRING.md)
  * [Code of Conduct](CODESTYLE.md)

Here you can find a constantly updated list of projects I'm working on,
as well as an additional notes which I've deicided to make public.

You can check out the [projects](PROJECTS.md) page and choose one or more to
participate in if you have some free time and desire to code. Just read the
[hiring](HIRING.md) section to get more details on how it works.

Use [gpg](GPG.md) if you want to send an encrypted message using a "ticket"
system or any other public messenger.

## Maintainers information

You're free to make a package for your operating system.
Every project contains some license files which you have 
to distribute with the program.

## Mirrors

Some free and open `git` hosting services, which use `gitlab` doesn't
work without `javascript`, so I don't use them unitl they fix the issue.

  - NotaBug  [https://notabug.org/g0tsu](https://notabug.org/g0tsu)
  - CodeBerg [https://codeberg.org/g0tsu](https://codeberg.org/g0tsu)

## Media

Nothing yet

## Platforms

It is a bit difficult to support all of them. The most supported platform is
`Linux`, so if you're a `xBSD` or any other open source platform user,
and want to improve something - please let me know.

## Anonymity

The free software concept has many advantages, one of them is anonymity.
For instance, you're using the same software for 5 different websites.
No matter if you've bought or made it by yourslef, important thing the software
is unique, nobody uses it but you. No PhD is needed to find out the connection 
between all of these websites with a single person.
The `Tor` project has the same concept - the more users the harder to
identify any of them.

## License information

All projects I make is under `GPLv2` license. I believe it is the most free
(as in freedom) and open source license in this world.

