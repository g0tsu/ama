[<< Table of contents](README.md)

# Make code, have fun

Making publically available projects opens many apportunities, one of them
is mutual interests. Practically speaking - everyone benefits participating
in development, it increases quality of code as well it brings some kind of
entertainment to everyday's coding routine.

## Requirements

  * Desire to make some code.
  * Knowledge of C programming language and desire to improve it.
  * English, at least written.

## Benefits

  * Anonymity, it means I wan't publish any information about you.
  * No racial, gender or any other kind of shit.
  * Skill sharing.

