# Projects

  * [curlftpfs3](#curlftpfs3)
  * [libcaptcha](#libcaptcha)
  * [fusezipfs3](#fusezipfs3)
  * [litemdview](#litemdview)
  * [md-cheatsheet](#md-cheatsheet)
  * [libhighlight](#libhighlight)
  * [Upcoming projects](#upcoming-projects)

## curlftpfs3

Mount ftp filesystems using libcurl and fuse3

Git repository: [curlftpfs3](https://notabug.org/g0tsu/curlftpfs3)

## libcaptcha

A C library that makes captcha.

Git repository: [libcaptcha](https://notabug.org/g0tsu/libcaptcha)

## fusezipfs3

Mount zip archives as a regular filesystem.

Git repository: [fusezipfs3](https://notabug.org/g0tsu/fusezipfs3)

## litemdview

A suckless markdown viewer

Git repository: [litemdview](https://notabug.org/g0tsu/litemdview)

## md cheatsheet

Markdown cheatsheet under GPLv2

Git repository: [md-cheatsheet](https://notabug.org/g0tsu/md-cheatsheet)

## libhighlight

Source code highlight library in C

Git repository [libhighlight](https://notabug.org/g0tsu/libhighlight)

## Upcoming projects

* SharePoint

Ftp server which runs on one of the essperif devices.

[<< Table of contents](README.md)
